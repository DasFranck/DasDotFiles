export ZSH="/home/das/.oh-my-zsh"

ZSH_THEME="geoffgarside"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(
  autojump
  cp
  git
)

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/oh-my-zsh.sh

unsetopt BEEP

export EDITOR="nvim"
export LANG=en_GB.UTF-8
alias ac="acpi | awk -F ', ' '{print \$2}'"
alias c="bat"
alias du="dust"
alias g!="git commit --amend --no-edit"
alias gdl="git diff HEAD^ HEAD"
alias gfu="git add . && git commit --amend --no-edit && git push --force"
alias gpt="git push --tags"
alias l="exa --long --header --git"
alias ls="exa"
alias ll="exa --long --header"
alias la="exa --long --header --all --group --git"
alias lar="exa --long --header --all --group --git --recurse"
alias lat="exa --long --header --all --group --git --tree"
alias ps="procs"
alias top="btm"
alias v="nvim"
alias vim="nvim"
alias zrc="source ./zshrc"

eval "$(starship init zsh)"
